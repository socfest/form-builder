<?php
/**
 * Created by PhpStorm.
 * User: ptibor
 * Date: 2019-01-15
 * Time: 19:09
 */

namespace Socfest\FormBuilder\Service;


use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Socfest\FormBuilder\Annotation\Form;
use Socfest\FormBuilder\Annotation\Number;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormConfigBuilderInterface;
use Symfony\Component\Form\FormConfigInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\RequestHandlerInterface;
use Symfony\Component\Form\ResolvedFormTypeInterface;
use Symfony\Component\PropertyAccess\PropertyPathInterface;
use Symfony\Component\VarDumper\VarDumper;

class EntityFormBuilder extends FormFactory implements ObjectFormBuilderInterface
{
    private $entity;
    /** @var EntityManager */
    private $em;

    /**
     * @param EntityManagerInterface $em
     * @required
     */
    public function setEntityManager(EntityManagerInterface $em) {
        $this->em = $em;
    }

    /**
     * @param $object
     * @param string $name
     * @param array $formOptions
     * @return \Symfony\Component\Form\FormBuilderInterface
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    public function createForm($object, $name = '', $formOptions = [])
    {
        $reader = new AnnotationReader();
        $reflectionObject = new \ReflectionObject($object);

        if ($name) {
            $builder = $this->createNamedBuilder($name, FormType::class, $object, $formOptions);
        } else {
            $builder = $this->createBuilder(FormType::class, $object, $formOptions);
        }

        $annotations = [];
        foreach ($reflectionObject->getProperties() as $property) {
            $baseOptions = [];
            /** @var Form $form */
            if (!$form = $reader->getPropertyAnnotation($property, Form::class)) {
                continue;
            }

            if ($column = $reader->getPropertyAnnotation($property, Column::class)) {
                if ($column->nullable == true) {
                    $baseOptions['required'] = false;
                }

                if (!$type = $form->getType()) {
                    switch ($column->type) {
                        case 'date':
                            $type = DateType::class;
                            $baseOptions['html5'] = true;
                            $baseOptions['widget'] = 'single_text';

                            break;
                        case 'datetime':
                            $type = DateTimeType::class;
                            $baseOptions['html5'] = true;
                            $baseOptions['widget'] = 'single_text';
                            break;
                        case 'integer':
                            $type = IntegerType::class;
                            break;
                        case 'boolean':
                            $baseOptions['required'] = false;
                            $type = CheckboxType::class;
                            break;
                        default:
                            $type = TextType::class;
                            break;
                    }
                }

                if (isset($form->options['choices'])) {
                    $this->prepareChoices($form->options['choices'], $object, $baseOptions);
                }

            } elseif (
                $column = $reader->getPropertyAnnotation($property, ManyToOne::class)
                or
                $column = $reader->getPropertyAnnotation($property, OneToMany::class)
                or
                $column = $reader->getPropertyAnnotation($property, OneToOne::class)
            ) {
                $type = EntityType::class;
                $baseOptions = [
                    'class' => $column->targetEntity,
                    'required' => false,
                    'empty_data' => null,
                    'placeholder' => '(nincs)'
                ];

                if (isset($form->options['choices'])) {
                    $this->prepareChoices($form->options['choices'], $column->targetEntity, $baseOptions);
                }
            } elseif (
                $column = $reader->getPropertyAnnotation($property, ManyToMany::class)
            ) {
                $type = CollectionType::class;
                $baseOptions = [
                    'entry_type' => EntityType::class,
                    'entry_options' => [
                        'class' => $column->targetEntity,
                        'required' => false,
                        'empty_data' => null,
                        'placeholder' => '(nincs)',
                    ],
                    'allow_add' => true,
                    'allow_delete' => true,
                    'prototype' => true,
                    'attr' => [
                        'class' => 'add-prototype'
                    ]
                ];

                if (isset($form->options['choices'])) {
                    $this->prepareChoices($form->options['choices'], $column->targetEntity, $baseOptions);
                }
            } else {
                continue;
            }

            $builder->add(
                $form->getName() ?: $property->getName(),
                $type,
                $baseOptions + $form->getOptions()
            );
        }

        return $builder;
    }

    /**
     * @param $choicesOption
     * @param $entity
     * @param array $baseOptions
     */
    protected function prepareChoices($choicesOption, $entity, array &$baseOptions): void
    {
        if (!is_array($choicesOption)) {
            list($funcType, $func) = explode(':', $choicesOption);
            switch ($funcType) {
                case "repo":
                    $baseOptions['choices'] = $this->em->getRepository($entity)->$func();
                    break;
                case "entity":
                    $baseOptions['choices'] = $entity::$func();
                    break;
                case "call":
                    $baseOptions['choices'] = call_user_func($func);
                    break;
            }
        }
    }
}
