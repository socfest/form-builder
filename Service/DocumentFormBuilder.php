<?php


namespace Socfest\FormBuilder\Service;


use Doctrine\Bundle\MongoDBBundle\Form\Type\DocumentType;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\ReferenceMany;
use Doctrine\ODM\MongoDB\Mapping\Annotations\ReferenceOne;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Socfest\FormBuilder\Annotation\Form;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\VarDumper\VarDumper;

class DocumentFormBuilder extends FormFactory
    implements ObjectFormBuilderInterface
{
    /**
     * @var DocumentManager
     */
    private $dm;

    /**
     * @param DocumentManager $dm
     * @required
     */
    public function setDocumentManager(DocumentManager $dm)
    {
        $this->dm = $dm;
    }

    /**
     * @param object $object
     * @param string $name
     * @param array $formOptions
     * @return \Symfony\Component\Form\FormBuilderInterface
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    public function createForm($object, $name = '', $formOptions = [])
    {
        $reader = new AnnotationReader();
        $reflectionObject = new \ReflectionObject($object);

        if ($name) {
            $builder = $this->createNamedBuilder($name, FormType::class, $object, $formOptions);
        } else {
            $builder = $this->createBuilder(FormType::class, $object, $formOptions);
        }
        $annotations = [];
        foreach ($reflectionObject->getProperties() as $property) {
            $baseOptions = [];
            /** @var Form $form */
            if (!$form = $reader->getPropertyAnnotation($property, Form::class)) {
                continue;
            }

            if ($column = $reader->getPropertyAnnotation($property, Field::class)) {
                $baseOptions['required'] = false;

                if (!$type = $form->getType()) {
                    switch ($column->type) {
                        case 'date':
                            $type = DateType::class;
                            $baseOptions['html5'] = true;
                            $baseOptions['widget'] = 'single_text';

                            break;
                        case 'datetime':
                            $type = DateTimeType::class;
                            $baseOptions['html5'] = true;
                            $baseOptions['widget'] = 'single_text';
                            break;
                        case 'integer':
                        case 'int':
                            $type = IntegerType::class;
                            break;
                        case 'boolean':
                        case 'bool':
                            $baseOptions['required'] = false;
                            $type = CheckboxType::class;
                            break;
                        default:
                            $type = TextType::class;
                            break;
                    }
                }

                if (isset($form->options['choices'])) {
                    $this->prepareChoices($form->options['choices'], $object, $baseOptions);
                }

            }







            elseif (
                $column = $reader->getPropertyAnnotation($property, ReferenceOne::class)
            ) {
                $type = DocumentType::class;
                $baseOptions = [
                    'class' => $column->targetDocument,
                    'required' => false,
                    'empty_data' => null,
                    'placeholder' => '(nincs)'
                ];

                if (isset($form->options['choices'])) {
                    $this->prepareChoices($form->options['choices'], $column->targetDocument, $baseOptions);
                }
            } elseif (
                $column = $reader->getPropertyAnnotation($property, ReferenceMany::class)
            ) {
                $type = CollectionType::class;
                $baseOptions = [
                    'entry_type' => DocumentType::class,
                    'entry_options' => [
                        'class' => $column->targetDocument,
                        'required' => false,
                        'empty_data' => null,
                        'placeholder' => '(nincs)',
                    ],
                    'allow_add' => true,
                    'allow_delete' => true,
                    'prototype' => true,
                    'attr' => [
                        'class' => 'add-prototype'
                    ]
                ];

                if (isset($form->options['choices'])) {
                    $this->prepareChoices($form->options['choices'], $column->targetDocument, $baseOptions);
                }
            } else {
                continue;
            }

            $builder->add(
                $form->getName() ?: $property->getName(),
                $type,
                $baseOptions + $form->getOptions()
            );
        }
        return $builder;
    }


    /**
     * @param $choicesOption
     * @param $entity
     * @param array $baseOptions
     */
    protected function prepareChoices($choicesOption, $entity, array &$baseOptions): void
    {
        if (!is_array($choicesOption)) {
            list($funcType, $func) = explode(':', $choicesOption);
            switch ($funcType) {
                case "repo":
                    $baseOptions['choices'] = $this->dm->getRepository($entity)->$func();
                    break;
                case "entity":
                    $baseOptions['choices'] = $entity::$func();
                    break;
                case "call":
                    $baseOptions['choices'] = call_user_func($func);
                    break;
            }
        }
    }
}
