<?php


namespace Socfest\FormBuilder\Service;


interface ObjectFormBuilderInterface
{
    public function createForm(object $object, $name = '', $formOptions = []);
}
