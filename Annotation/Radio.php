<?php
/**
 * Created by PhpStorm.
 * User: ptibor
 * Date: 2019-01-18
 * Time: 16:41
 */

namespace Socfest\FormBuilder\Annotation;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\VarDumper\VarDumper;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 */
class Radio extends Form
{
    public $type = ChoiceType::class;

    /**
     * @return array
     */
    public function getOptions(): array
    {
        $this->options['expanded'] = true;

        return $this->options;
    }
}