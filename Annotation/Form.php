<?php
/**
 * Created by PhpStorm.
 * User: ptibor
 * Date: 2019-01-15
 * Time: 18:58
 */

namespace Socfest\FormBuilder\Annotation;

use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 */
class Form
{
    public $name = '';
    public $type = '';
    public $options = [];

    public function __construct(array $values)
    {
        foreach ($values as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }
}