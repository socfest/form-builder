<?php
/**
 * Created by PhpStorm.
 * User: ptibor
 * Date: 2019-01-15
 * Time: 20:31
 */

namespace Socfest\FormBuilder\Annotation;


use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 */
class Integer extends Form
{
    public $type = IntegerType::class;
}