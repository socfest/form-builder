<?php
/**
 * Created by PhpStorm.
 * User: ptibor
 * Date: 2019-01-18
 * Time: 16:41
 */

namespace Socfest\FormBuilder\Annotation;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\VarDumper\VarDumper;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 */
class Choices extends Form
{
    public $type = ChoiceType::class;
}