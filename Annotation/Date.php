<?php
/**
 * Created by PhpStorm.
 * User: ptibor
 * Date: 2019-01-16
 * Time: 11:04
 */

namespace Socfest\FormBuilder\Annotation;


use Symfony\Component\Form\Extension\Core\Type\DateType;

/**
 * @Annotation
 * @Target({"PROPERTY"})
 */
class Date extends Form
{
    public $type = DateType::class;
    public $html5 = true;

    /**
     * @return array
     */
    public function getOptions(): array
    {
        if ($this->html5) {
            $this->options['html5'] = true;
            $this->options['widget'] = 'single_text';
        }
        return $this->options;
    }
}